#pragma once
#include <iostream>
#include <cmath>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <cstdlib>
#include <iostream>

//Пространство имён.
namespace first_space {

    class Quaternion {
    public:
        double w, x, y, z;

        Quaternion() {};

        Quaternion(double nx, double ny, double nz);

        Quaternion(double nw, double nx, double ny, double nz);

        Quaternion operator+(Quaternion b);

        Quaternion operator-(Quaternion b);

        Quaternion operator*(double b);

        Quaternion operator*(Quaternion b);

        Quaternion operator/(double b);

        Quaternion inverse();

        double get_magnitude();
        void set_magnitude(double newmag);

        void normalize();

        Quaternion normalized();

        sf::Vector2f getScreenPos();

        Quaternion clipLineToScreen(Quaternion A, Quaternion B);

    };

}

