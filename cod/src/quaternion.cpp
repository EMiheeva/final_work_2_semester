#pragma once
#include <iostream>
#include <cmath>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <cstdlib>
#include <quaternion.hpp>
#include <const.hpp>
#include <iostream>

namespace first_space {

    Quaternion::Quaternion(double nx, double ny, double nz) {
        w = 0;
        x = nx;
        y = ny;
        z = nz;
    }

    Quaternion::Quaternion(double nw, double nx, double ny, double nz) {
        w = nw;
        x = nx;
        y = ny;
        z = nz;
    }

    Quaternion Quaternion::operator+(Quaternion b) {
        return Quaternion(w + b.w, x + b.x, y + b.y, z + b.z);
    }

    Quaternion Quaternion::operator-(Quaternion b) {
        return Quaternion(w - b.w, x - b.x, y - b.y, z - b.z);
    }

    Quaternion Quaternion::operator*(double b) {
        return Quaternion(b * w, b * x, b * y, b * z);
    }

    Quaternion Quaternion::operator*(Quaternion b) {
        return Quaternion(w * b.w - x * b.x - y * b.y - z * b.z,
            w * b.x + x * b.w + y * b.z - z * b.y,
            w * b.y - x * b.z + y * b.w + z * b.x,
            w * b.z + x * b.y - y * b.x + z * b.w);
    }

    Quaternion Quaternion::operator/(double b) {
        return Quaternion(w / b, x / b, y / b, z / b);
    }

    Quaternion Quaternion::inverse() {
        return Quaternion(w, -x, -y, -z) / get_magnitude();
    }

    double Quaternion::get_magnitude() {
        return std::pow(std::pow(w, 2) + std::pow(x, 2) + std::pow(y, 2) + std::pow(z, 2), 0.5);
    }

    void Quaternion::set_magnitude(double newmag) {
        double factor = newmag / get_magnitude();

        w = w * factor;
        x = x * factor;
        y = y * factor;
        z = z * factor;

    }

    void Quaternion::normalize() {
        set_magnitude(1.);
    }

    Quaternion Quaternion::normalized() {
        Quaternion Q(w, x, y, z);
        Q.normalize();
        return Q;
    }

    sf::Vector2f Quaternion::getScreenPos() {
        if (z == 0)
            return sf::Vector2f(-1000, -1000);
        return sf::Vector2f(
            ratio * (screenwidth / 2.0) * x / z + (screenwidth / 2.0),
            ratio * (screenheight / 2.0) * y / z + (screenheight / 2.0));
    }

    auto clipLineToScreen(first_space::Quaternion A, first_space::Quaternion B) {
        if (A.z > 0)
            return A;
        else {
            return A + (B - A) * ((0.0001 - A.z) / (B.z - A.z));
        }
    }
};







