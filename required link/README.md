# Полезные ссылки на сайты и информационные ресурсы в одном месте:

### 1) ссылка на [репозиторий с sfml-sample] 

[репозиторий с sfml-sample]: https://gitlab.com/EMiheeva/sfml_sample
### 2) ссылка на [онлайн-конвертатор из png в icon]

[онлайн-конвертатор из png в icon]: https://convertio.co/ru/png-ico/

### 3) что такое [библиотека cstdlib]?
[библиотека cstdlib]: https://cplusplus.com/reference/cstdlib/

### 4) что такое [тернарный оператор]?
[тернарный оператор]: http://cppstudio.com/post/304/

### 5) разница между [const и constexpr]
[const и constexpr]: https://www.stackfinder.ru/questions/14116003/difference-between-constexpr-and-const

### 6) как [убрать консольное окно] при запуске Debug или Release?

[убрать консольное окно]: https://ru.stackoverflow.com/questions/482568/%D0%9A%D0%B0%D0%BA-%D1%83%D0%B1%D1%80%D0%B0%D1%82%D1%8C-%D0%BA%D0%BE%D0%BD%D1%81%D0%BE%D0%BB%D1%8C%D0%BD%D0%BE%D0%B5-%D0%BE%D0%BA%D0%BD%D0%BE-%D0%BD%D0%B0-%D1%81%D1%82%D0%B0%D1%80%D1%82%D0%B5-%D0%B1%D0%B5%D0%B7-%D0%B8%D1%81%D0%BF%D0%BE%D0%BB%D1%8C%D0%B7%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F-winmain




