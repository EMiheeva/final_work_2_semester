﻿#include <iostream>
#include <cmath>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <cstdlib>
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

const int angle = 90;
const double ratio = std::tan(angle / 2.0);
const int screenwidth = 1000;
const int screenheight = 850;
const sf::Vector2i size(screenwidth, screenheight);
const float power = 0.003;

class Quaternion {
public:
    double w, x, y, z;

    Quaternion() {};

    Quaternion(double nx, double ny, double nz) {
        w = 0;
        x = nx;
        y = ny;
        z = nz;
    }

    Quaternion(double nw, double nx, double ny, double nz) {
        w = nw;
        x = nx;
        y = ny;
        z = nz;
    }

    Quaternion operator+(Quaternion b) {
        return Quaternion(w + b.w, x + b.x, y + b.y, z + b.z);
    }

    Quaternion operator-(Quaternion b) {
        return Quaternion(w - b.w, x - b.x, y - b.y, z - b.z);
    }

    Quaternion operator*(double b) {
        return Quaternion(b * w, b * x, b * y, b * z);
    }

    Quaternion operator*(Quaternion b) {
        return Quaternion(w * b.w - x * b.x - y * b.y - z * b.z,
            w * b.x + x * b.w + y * b.z - z * b.y,
            w * b.y - x * b.z + y * b.w + z * b.x,
            w * b.z + x * b.y - y * b.x + z * b.w);
    }

    Quaternion operator/(double b) {
        return Quaternion(w / b, x / b, y / b, z / b);
    }

    Quaternion inverse() {
        return Quaternion(w, -x, -y, -z) / get_magnitude();
    }

    double get_magnitude() {
        return std::pow(std::pow(w, 2) + std::pow(x, 2) + std::pow(y, 2) + std::pow(z, 2), 0.5);
    }

    void set_magnitude(double newmag) {
        double factor = newmag / get_magnitude();

        w = w * factor;
        x = x * factor;
        y = y * factor;
        z = z * factor;

    }

    void normalize() {
        set_magnitude(1.);
    }

    Quaternion normalized() {
        Quaternion Q(w, x, y, z);
        Q.normalize();
        return Q;
    }

    sf::Vector2f getScreenPos() {
        if (z == 0)
            return sf::Vector2f(-1000, -1000);
        return sf::Vector2f(
            ratio * (screenwidth / 2.0) * x / z + (screenwidth / 2.0),
            ratio * (screenheight / 2.0) * y / z + (screenheight / 2.0));
    }
};

Quaternion clipLineToScreen(Quaternion A, Quaternion B) {
    if (A.z > 0)
        return A;
    else {
        return A + (B - A) * ((0.0001 - A.z) / (B.z - A.z));
    }
}

class Drawable {
public:
    double distanceFromCamera;
    bool shouldDraw;
    Drawable* next;
    Drawable* child;
    virtual void predraw(Quaternion camerapos, Quaternion camerarotation) {};
    virtual void draw(sf::RenderWindow& window) {};

    Drawable() {
        next = NULL;
        child = NULL;
        distanceFromCamera = -1;
    }

    virtual void insert(Drawable* item) {
        item->next = next;
        next = item;
    }

};

class Particle : public Drawable {

public:
    Particle(Quaternion _pos, const std::string& spritePath, float _scale) {
        pos = _pos;
        scale = _scale;
        if (!texture.loadFromFile(spritePath)) {
            std::cout << "No pictures!" << std::endl;
        }
        back = new sf::Sprite(texture);
    }

    void draw(sf::RenderWindow& window) {
        window.draw(*back);
    }

    void predraw(Quaternion camerapos, Quaternion camerarotation) {
        draw_pos = camerarotation * ((pos - camerapos) * camerarotation.inverse());
        distanceFromCamera = draw_pos.get_magnitude();
        back->setPosition(draw_pos.getScreenPos());
        back->setScale(scale, scale);
    }

private:
    sf::Sprite* back;
    Quaternion pos;
    Quaternion draw_pos;
    sf::Texture texture;
    float scale;
};

class Sphere : public Drawable
{
public:
    Quaternion pos;

    Sphere(Quaternion _pos, double _radius, sf::Color _color) {
        pos = _pos;
        radius = _radius;
        color = _color;

        next = NULL;
        child = NULL;
    }

    void predraw(Quaternion camerapos, Quaternion camerarotation) { //
        draw_pos = camerarotation * ((pos - camerapos) * camerarotation.inverse());

        distanceFromCamera = draw_pos.get_magnitude();

        render_radius = radius / distanceFromCamera;

        shape.setFillColor(color);

        if (draw_pos.z < 0)
            shape.setRadius(0);
        else
            shape.setRadius(render_radius);

        shape.setPosition(draw_pos.getScreenPos() + sf::Vector2f(-render_radius, -render_radius));
    }

    void draw(sf::RenderWindow& window) {
        window.draw(shape);
    }

private:
    double radius;
    double render_radius;
    Quaternion draw_pos;
    sf::Color color;
    sf::CircleShape shape;
};

class Comet : public Drawable {

public:
    Comet(Quaternion _pos, const std::string& spritePath, float _scale, float _speed_x, float _speed_y) {
        pos = _pos;
        scale = _scale;
        speed_x = _speed_x;
        speed_y = _speed_y;
        if (!texture.loadFromFile(spritePath)) {
            std::cout << "No pictures!" << std::endl;
        }
        back = sf::Sprite(texture);
    }

    void draw(sf::RenderWindow& window) {
        window.draw(back);
    }

    void predraw(Quaternion camerapos, Quaternion camerarotation) {

        pos.x = pos.x + speed_x;
        if (pos.x > screenwidth) {
            pos.x = 0;
        }
        pos.y = pos.y + speed_y;
        if (pos.y > screenheight) {
            pos.y = 0;
        }
        draw_pos = camerarotation * ((pos - camerapos) * camerarotation.inverse());
        distanceFromCamera = draw_pos.get_magnitude();
        back.setPosition(draw_pos.getScreenPos());
        back.setScale(scale, scale);
    }

private:
    sf::Sprite back;
    Quaternion pos;
    Quaternion draw_pos;
    sf::Texture texture;
    float scale;
    float speed_x;
    float speed_y;

};

//START
Drawable* mergeSort(Drawable*);
Drawable* merge(Drawable*, Drawable*);
Drawable* split(Drawable*);


Drawable* mergeSort(Drawable* start) {
    Drawable* second;

    if (start == NULL)
        return NULL;
    else if (start->next == NULL)
        return start;
    else
    {
        second = split(start);
        return merge(mergeSort(start), mergeSort(second));
    }
}

Drawable* merge(Drawable* first, Drawable* second) {

    if (first == NULL) return second;
    else if (second == NULL) return first;
    else if (first->distanceFromCamera >= second->distanceFromCamera)
    {
        first->next = merge(first->next, second);
        return first;
    }
    else
    {
        second->next = merge(first, second->next);
        return second;
    }
}

Drawable* split(Drawable* start) {
    Drawable* second;

    if (start == NULL) return NULL;
    else if (start->next == NULL) return NULL;
    else {
        second = start->next;
        start->next = second->next;
        second->next = split(second->next);
        return second;
    }
}
//END


void predraw_list(Drawable*& start, Quaternion camerapos, Quaternion camerarotation) {
    Drawable* iter = start;
    int length = 0;

    while (iter != NULL) {
        iter->predraw(camerapos, camerarotation);
        iter = iter->next;
        length++;
    }

    start = mergeSort(start);
}

void draw_list(Drawable* start, sf::RenderWindow& window) {
    Drawable* iter = start;
    while (iter != NULL) {
        iter->draw(window);
        iter = iter->next;
    }
}

Quaternion particle_random_position() {
    const int range = 100;
    return Quaternion(std::rand() % range, std::rand() % range, std::rand() % range);
}

int main() {
    sf::ContextSettings settings;
    settings.antialiasingLevel = 8;

    sf::RenderWindow window(sf::VideoMode(screenwidth, screenheight), "Airy and frosty atmosphere", sf::Style::Default, settings);
    sf::Event        event;

    sf::Texture texture;
    if (!texture.loadFromFile("background.jpg")) {
        std::cout << "No pictures!" << std::endl;
        return -1;
    }
    sf::Sprite back;
    back.setTexture(texture);

    window.setMouseCursorVisible(false);

    std::cout << "start" << std::endl;

    std::cout << "end" << std::endl;

    Drawable* objects = new Drawable;

    std::rand();

    constexpr float PARTICLE_SCALE_MIN = 0.5;
    constexpr float PARTICLE_SCALE_MAX = 2;
    constexpr int PARTICLE_COUNT = 100;

    for (int i = 0; i < PARTICLE_COUNT; i++) {
        auto spritePath = std::rand() % 2 > 0 ? "snow.png" : "snow2.png";
        auto initialPosition = particle_random_position();
        auto scale = PARTICLE_SCALE_MIN + (float)(rand()) / ((float)(RAND_MAX / (PARTICLE_SCALE_MAX - PARTICLE_SCALE_MIN)));
        objects->insert(new Particle(initialPosition, spritePath, scale));
    }

    constexpr float COMET_SCALE_MIN = 0.5;
    constexpr float COMET_SCALE_MAX = 2;

    constexpr float COMET_SPEED_MIN = 0.05;
    constexpr float COMET_SPEED_MAX = 0.1;

    constexpr int COMET_COUNT = 10;

    for (int i = 0; i < COMET_COUNT; i++) {
        auto initialPosition = particle_random_position();
        auto scale = COMET_SCALE_MIN + (float)(rand()) / ((float)(RAND_MAX / (COMET_SCALE_MAX - COMET_SCALE_MIN)));
        auto speed_x = COMET_SPEED_MIN + (float)(rand()) / ((float)(RAND_MAX / (COMET_SPEED_MAX - COMET_SPEED_MIN)));
        auto speed_y = COMET_SPEED_MIN + (float)(rand()) / ((float)(RAND_MAX / (COMET_SPEED_MAX - COMET_SPEED_MIN)));
        auto comet = new Comet(initialPosition, "flake.png", scale, speed_x, speed_y);
        objects->insert(comet);
    }

    Quaternion camerapos(0, 0, 0, 0);
    Quaternion cameravel(0, 0, 0, 0);
    Quaternion camerarotation(Quaternion(1, 0, 0, 0).normalized());
    Quaternion temprotation(0, 0, 0, 0);

    int oldMouseX = screenwidth / 2;
    int oldMouseY = screenheight / 2;
    int dMouseX;
    int dMouseY;

    sf::Mouse::setPosition(sf::Vector2i(screenwidth / 2, screenheight / 2), window);

	sf::Image icon;
    if (!icon.loadFromFile("calendar.png")) {
        return 1;
    }
    window.setIcon(32, 32, icon.getPixelsPtr());

    while (window.isOpen()) {
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();


            if (event.type == sf::Event::KeyPressed) {
            }

            if (event.type == sf::Event::MouseMoved) {
                dMouseX = event.mouseMove.x - oldMouseX;
                dMouseY = event.mouseMove.y - oldMouseY;

                if ((dMouseX != 0) || (dMouseY != 0)) {
                    temprotation = Quaternion(1, 0.001 * dMouseY, -0.001 * dMouseX, 0).normalized();
                    camerarotation = temprotation * camerarotation;

                    sf::Mouse::setPosition(sf::Vector2i(screenwidth / 2, screenheight / 2), window);

                    oldMouseX = screenwidth / 2;
                    oldMouseY = screenheight / 2;
                }
            }
        }

        camerarotation.normalize();

        cameravel = cameravel + camerarotation.inverse() * (Quaternion(0, 0, 0, 0.01 * (double)sf::Keyboard::isKeyPressed(sf::Keyboard::W)) * camerarotation);
        cameravel = cameravel + camerarotation.inverse() * (Quaternion(0, 0, 0, -0.01 * (double)sf::Keyboard::isKeyPressed(sf::Keyboard::S)) * camerarotation);

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
            cameravel = cameravel * 0.95;

        camerapos = camerapos + cameravel;

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q)) {
            camerarotation = Quaternion(1, 0, 0, power).normalized() * camerarotation;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::E)) {
            camerarotation = Quaternion(1, 0, 0, -power).normalized() * camerarotation;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
            window.close();
            std::cout << "Good job!" << std::endl;
        }

        window.clear(sf::Color(0, 0, 0));

        window.draw(back);

        predraw_list(objects, camerapos, camerarotation);

        draw_list(objects, window);

        window.display();
    }

    delete objects;

    return 0;
}
