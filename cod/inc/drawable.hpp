#pragma once
#include <iostream>
#include <cmath>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <cstdlib>
#include <iostream>
#include <quaternion.hpp>
#include <const.hpp>

//Главный класс, создающий камеру.
class Drawable {
public:
    double distanceFromCamera;
    bool shouldDraw;
    Drawable* next;
    Drawable* child;
    virtual void predraw(first_space::Quaternion camerapos, first_space::Quaternion camerarotation) {};
    virtual void draw(sf::RenderWindow& window) {};

    Drawable() {
        next = NULL;
        child = NULL;
        distanceFromCamera = -1;
    }

    virtual void insert(Drawable* item) {
        item->next = next;
        next = item;
    }

};

//Класс-наследник.
//Частица превратится в снежинку.
//Вокруг них можно будет вращать камеру.
class Particle : public Drawable {

public:
    Particle(first_space::Quaternion _pos, const std::string& spritePath, float _scale) {
        pos = _pos;
        scale = _scale;
		//В main.cpp укажем саму картинку с расширением.
        if (!texture.loadFromFile(spritePath)) {
            std::cout << "There is no picture! Add it!" << std::endl;
        }
        back = new sf::Sprite(texture);
    }

    void draw(sf::RenderWindow& window) {
        window.draw(*back);
    }

    void predraw(first_space::Quaternion camerapos, first_space::Quaternion camerarotation) {
        draw_pos = camerarotation * ((pos - camerapos) * camerarotation.inverse());
        distanceFromCamera = draw_pos.get_magnitude();
        back->setPosition(draw_pos.getScreenPos());
        back->setScale(scale, scale);
    }

private:
    sf::Sprite* back;
    first_space::Quaternion pos;
    first_space::Quaternion draw_pos;
    sf::Texture texture;
    float scale;
};

//Класс-наследник.
//Эти снежинки будут самостоятельно двигаться
//во время вращения камеры вокруг них.
class Flake : public Drawable {

public:
    Flake(first_space::Quaternion _pos, const std::string& spritePath, float _scale, float _speed_x, float _speed_y) {
        pos = _pos;
        scale = _scale;
        speed_x = _speed_x;
        speed_y = _speed_y;
        if (!texture.loadFromFile(spritePath)) {
            std::cout << "There is no picture! Add it!" << std::endl;
        }
        back = sf::Sprite(texture);
    }

    void draw(sf::RenderWindow& window) {
        window.draw(back);
    }

    void predraw(first_space::Quaternion camerapos, first_space::Quaternion camerarotation) {

        pos.x = pos.x + speed_x;
        if (pos.x > screenwidth) {
            pos.x = 0;
        }
        pos.y = pos.y + speed_y;
        if (pos.y > screenheight) {
            pos.y = 0;
        }
        draw_pos = camerarotation * ((pos - camerapos) * camerarotation.inverse());
        distanceFromCamera = draw_pos.get_magnitude();
        back.setPosition(draw_pos.getScreenPos());
        back.setScale(scale, scale);
    }


private:
    sf::Sprite back;
    first_space::Quaternion pos;
    first_space::Quaternion draw_pos;
    sf::Texture texture;
    float scale;
    float speed_x;
    float speed_y;

};


//Работа камеры.
Drawable* mergeSort(Drawable*);
Drawable* merge(Drawable*, Drawable*);
Drawable* split(Drawable*);


Drawable* mergeSort(Drawable* start) {
    Drawable* second;

    if (start == NULL)
        return NULL;
    else if (start->next == NULL)
        return start;
    else
    {
        second = split(start);
        return merge(mergeSort(start), mergeSort(second));
    }
}

Drawable* merge(Drawable* first, Drawable* second) {

    if (first == NULL) return second;
    else if (second == NULL) return first;
    else if (first->distanceFromCamera >= second->distanceFromCamera) 
    {
        first->next = merge(first->next, second);
        return first;
    }
    else
    {
        second->next = merge(first, second->next);
        return second;
    }
}

Drawable* split(Drawable* start) {
    Drawable* second;

    if (start == NULL) return NULL;
    else if (start->next == NULL) return NULL;
    else {
        second = start->next;
        start->next = second->next;
        second->next = split(second->next);
        return second;
    }
}
//Конец работы камеры.


void predraw_list(Drawable*& start, first_space::Quaternion camerapos, first_space::Quaternion camerarotation) {
    Drawable* iter = start;
    int length = 0;

    while (iter != NULL) {
        iter->predraw(camerapos, camerarotation);
        iter = iter->next;
        length++;
    }

    start = mergeSort(start);
}

void draw_list(Drawable* start, sf::RenderWindow& window) {
    Drawable* iter = start;
    while (iter != NULL) {
        iter->draw(window);
        iter = iter->next;
    }
}

