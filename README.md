# semester_2_final_project #
## (итоговая работа по программированию) ##
### Краткая информация о проекте: ###
В данной программе можно двигать снежинки различных форм и размеров, 
самостоятельно задавать скорость, их количество и вид.
Пользователь может изменить идею: создать новые параметры объектов, фона и иконки.

### Соответствие с критериями:
в прикрепленном файле "Итоговая работа" с расширением .docx 
выделены выполненные требования к проекту.

### Путеводитель по репозиторию: ###
1) backup cod - папка с резервным кодом в одном .cpp файле;
2) cod - главная папка с кодом и файлами проекта;
3) documentation - папка с документацией для пользователя;
4) required link - папка с файлом, содержащим ссылки;
5) файл .docx с требованиями к проектной работе.

### Отчёт по проектной работе: ###
Рассмотрены и использованы методы и темы:
1) ООП: распределение классов по файлам, 
создание отдельного файла для констант;
2) Указатели для использования динамической памяти;
3) Наследование для улучшения производительности;
4) Переименование переменных и констант для упрощения
понимания кода;
5) Добавление кнопок для взаимодействия с программой;
6) Упрощенный выход из программы с помощью соответствующей
кнопки espace;
7) Попытка написать короткие комментарии в коде программы, 
чтобы не испортить восприятие пользователя;
8) Создание отдельной документацией с алгоритмом(инструкцией),
с ссылками на ресурсы с помощью файлов Readme.md;
9) Создание резервной копии кода и файлов(картинок, иконок)
для пользователя в случае их утери. Написана инструкция к ним;
10) Размещение работы в репозитории на сайте GitLab;
11) Предложение улучшения программы или изменения её
под личные вкусовые предпочтения.




