﻿#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")//Отключение консоли и вывода(cout).
//Если закомментировать, то выводы(cout) будут работать.
#include <iostream>
#include <cmath>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <cstdlib>//Библиотека для функции rand - генератор случайных чисел.
#include <quaternion.hpp>//Класс, создающий камеру
#include <drawable.hpp>//Класс, создающий камеру и отрисовывающий объекты
#include <const.hpp> //Все константы в отдельном файле

first_space::Quaternion particle_random_position() {
    const int range = 100;
    return first_space::Quaternion(std::rand() % range, std::rand() % range, std::rand() % range);
}
//first_space - пространство имен (namespace),
//в котором расположен класс Quaternion.
//генерирует позицию частицы-снежинки(participle) с координатами (x, y, z).


int main() {

    sf::RenderWindow window(sf::VideoMode(screenwidth, screenheight), "Airy and frosty atmosphere");
    sf::Event        event;

    sf::Texture texture;
    if (!texture.loadFromFile("background.jpg")) {
        std::cout << "There is no picture! Add it!" << std::endl;
        return -1;
    }
    sf::Sprite back;
    back.setTexture(texture);

    window.setMouseCursorVisible(false);

    std::cout << "start" << std::endl;
    std::cout << "end" << std::endl;

    Drawable* objects = new Drawable;
	
	//Инициализация генератора.
    std::rand();

	//Справка:
	//const - значение не будет изменяться. 
	//используется для данных, которые передаются в функции и методы так, 
	//чтобы они не изменились.
	//constexpr - вычисление константы во время компиляции. 
	//используется для размещения данных в памяти, где они не будут повреждены, 
	//а также для улучшения производительности.

    constexpr float PARTICLE_SCALE_MIN = 0.5;
    constexpr float PARTICLE_SCALE_MAX = 2;
    constexpr int PARTICLE_COUNT = 300;
    
	//Создаем частицы(participle), т.е. снежинки.
	//Это один объект, но в нем меняются два спрайта, создавая впечатление,
	//что объектов два.
	//это улучшает производительность.
	//для этого используется тернарный оператор "? :"
	
    for (int i = 0; i < PARTICLE_COUNT; i++) {
        auto spritePath = std::rand() % 2 > 0 ? "snow.png" : "snow2.png";
        auto initialPosition = particle_random_position();
        auto scale = PARTICLE_SCALE_MIN + (float)(rand()) / ((float)(RAND_MAX / (PARTICLE_SCALE_MAX - PARTICLE_SCALE_MIN)));
        objects->insert(new Particle(initialPosition, spritePath, scale));
    }
	
    constexpr float FLAKE_SCALE_MIN = 1;
    constexpr float FLAKE_SCALE_MAX = 2;

    constexpr float FLAKE_SPEED_MIN = 0.01;
    constexpr float FLAKE_SPEED_MAX = 0.5;

    constexpr int FLAKE_COUNT = 50;

	//Создаем самостоятельно(без клавиш) движущиеся снежинки,
	//задаем им начальную позицию, спрайт, размер, скорость по х и н
    for (int i = 0; i < FLAKE_COUNT; i++) {
        auto initialPosition = particle_random_position();
        auto scale = FLAKE_SCALE_MIN + (float)(rand()) / ((float)(RAND_MAX / (FLAKE_SCALE_MAX - FLAKE_SCALE_MIN)));
        auto speed_x = FLAKE_SPEED_MIN + (float)(rand()) / ((float)(RAND_MAX / (FLAKE_SPEED_MAX - FLAKE_SPEED_MIN)));
        auto speed_y = FLAKE_SPEED_MIN + (float)(rand()) / ((float)(RAND_MAX / (FLAKE_SPEED_MAX - FLAKE_SPEED_MIN)));
        auto flake = new Flake(initialPosition, "flake.png", scale, speed_x, speed_y);
        objects->insert(flake);
    }

    first_space::Quaternion camerapos(0, 0, 0, 0);
    first_space::Quaternion cameravel(0, 0, 0, 0);
    first_space::Quaternion camerarotation(first_space::Quaternion(1, 0, 0, 0).normalized());
    first_space::Quaternion temprotation(0, 0, 0, 0);

    int oldMouseX = screenwidth / 2;
    int oldMouseY = screenheight / 2;
    int dMouseX;
    int dMouseY;

    sf::Mouse::setPosition(sf::Vector2i(screenwidth / 2, screenheight / 2), window);

    sf::Image icon;
    if (!icon.loadFromFile("calendar.png")) {
        return 1;
    }
    window.setIcon(32, 32, icon.getPixelsPtr());

	//Результат(Event - событие) всей работы выводится на экран.
    while (window.isOpen()) {
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();

            if (event.type == sf::Event::KeyPressed) {
            }

            if (event.type == sf::Event::MouseMoved) {
                dMouseX = event.mouseMove.x - oldMouseX;
                dMouseY = event.mouseMove.y - oldMouseY;

                if ((dMouseX != 0) || (dMouseY != 0)) {
                    temprotation = first_space::Quaternion(1, 0.001 * dMouseY, -0.001 * dMouseX, 0).normalized();
                    camerarotation = temprotation * camerarotation;

                    sf::Mouse::setPosition(sf::Vector2i(screenwidth / 2, screenheight / 2), window);

                    oldMouseX = screenwidth / 2;
                    oldMouseY = screenheight / 2;
                }
            }
        }

        camerarotation.normalize();

        cameravel = cameravel + camerarotation.inverse() * (first_space::Quaternion(0, 0, 0, 0.01 * (double)sf::Keyboard::isKeyPressed(sf::Keyboard::W)) * camerarotation);
        cameravel = cameravel + camerarotation.inverse() * (first_space::Quaternion(0, 0, 0, -0.01 * (double)sf::Keyboard::isKeyPressed(sf::Keyboard::S)) * camerarotation);

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
            cameravel = cameravel * 0.95;

        camerapos = camerapos + cameravel;

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q)) {
            camerarotation = first_space::Quaternion(1, 0, 0, power).normalized() * camerarotation;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::E)) {
            camerarotation = first_space::Quaternion(1, 0, 0, -power).normalized() * camerarotation;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
            window.close();
			std::cout << "Good job!" << std::endl;
        }

        window.clear(sf::Color(0, 0, 0));

        window.draw(back);

        predraw_list(objects, camerapos, camerarotation);

        draw_list(objects, window);

        window.display();
    }
	//Конец программы.
    delete objects;
	//Очистка памяти для улучшения производительности.

    return 0;
}
