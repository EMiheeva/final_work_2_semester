#if !defined(MYLIB_CONSTANTS_H)
#define MYLIB_CONSTANTS_H 1

const int angle = 90;
const double ratio = std::tan(angle / 2.0);
const int screenwidth = 1000;
const int screenheight = 850;
const sf::Vector2i size(screenwidth, screenheight);
const float power = 0.003;

#endif